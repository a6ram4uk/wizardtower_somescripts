﻿using System.Collections.Generic;
using BezierSolution;
using UnityEngine;

[System.Serializable]
public enum ProjectileType
{
	Player,
	Wizard,
	Fireball
}

[System.Serializable]
public struct Projectile
{
	public ProjectileType type;
	public GameObject projectile;
	public GameObject flash;
}

public class SendProjectilesManager : MonoBehaviour
{
	public static SendProjectilesManager Instance;
	private void Awake () { Instance = this; }

	[Header ("Ссылки на точки полета партиклов: ____________________")]
	public Transform playerStartPathPoint_1;
	public Transform playerStartPathPoint_2;
	public Transform playerFinishPathPoint;
	public Transform bossStartPathPoint;
	public Transform bossFinishPathPoint;

	[Header ("Min и Max кол-во поворотов пути: _____________________")]
	[SerializeField, Range (0, 5)] private int numberOfSegments_Min = 1;
	[SerializeField, Range (0, 5)] private int numberOfSegments_Max = 2;

	[Header ("Параметры движения: __________________________________")]
	[SerializeField] private float travelTime = 1.0f;
	public float TravelTime { get { return travelTime; } }

	[SerializeField] private float movementLerpModifier = 50.0f;
	[SerializeField] private float rotationLerpModifier = 50.0f;
	[SerializeField] private bool lookForward = false;

	[Header ("Массив префабов: _____________________________________")]
	[SerializeField] private Projectile[] projectilesPrefabs;

	// >>> ========================================= >>>

	public void SendProjectiles (ProjectileType p_projectileType, Vector3[] p_startPoints, Vector3 p_finishPoint, float p_travelTime, int p_firstPerpFlag, int p_numberOfSegments_Min, int p_numberOfSegments_Max)
	{
		if (projectilesPrefabs.Length == 0)
		{
			Debug.Log ("projectilesPrefabs.Length == 0");
			return;
		}

		if (p_startPoints.Length > 0)
		{
			GameObject pathwaysInst = new GameObject ("Pathways");
			BezierWalkerWithTime[] walkers = CreateWalkers (p_projectileType, p_startPoints, p_finishPoint, p_travelTime, p_firstPerpFlag, p_numberOfSegments_Min, p_numberOfSegments_Max);
			foreach (var walker in walkers)
			{
				walker.transform.parent = pathwaysInst.transform;
				walker.spline.transform.parent = pathwaysInst.transform;
			}

			if (walkers.Length > 0)
			{
				// Выберем flash prefab из массива
				GameObject l_flashPrefab = null;
				for (int i_prefab = 0; i_prefab < projectilesPrefabs.Length; i_prefab++)
				{
					if (projectilesPrefabs[i_prefab].type == p_projectileType)
					{
						l_flashPrefab = projectilesPrefabs[i_prefab].flash;
						break;
					}

					else if (i_prefab == projectilesPrefabs.Length - 1)
						Debug.Log ("p_projectileType <" + p_projectileType + "> not found in projectilesPrefabs[]");
				}

				// Событие, срабатывающее когда projectile долетел
				walkers[0].onPathCompleted.AddListener (() =>
				{
					// Дефолтное время уничтожения всей системы, если вспышка отсутствует
					float destroyTime = 2.0f;

					// Создание вспышки
					if (l_flashPrefab)
					{
						GameObject flashInst = Instantiate (l_flashPrefab, p_finishPoint, Quaternion.identity, pathwaysInst.transform);
						flashInst.name = "Flash";

						destroyTime = flashInst.GetComponent<ParticleSystem> ().main.duration;
					}

					// Отключить child долетевших projectiles (но оставить их child Trail)
					foreach (var walker in walkers)
					{
						if (walker.transform.childCount > 1)
							walker.transform.GetChild (0).gameObject.SetActive (false);
					}

					// Уничтожить всю систему через время, равное жизни вспышки, если она была
					Destroy (pathwaysInst, destroyTime);

					if (LevelManaManager.Instance)
						LevelManaManager.Instance.Switch_IsManaSmoothing (true);
				});
			}
		}
	}

	private BezierWalkerWithTime[] CreateWalkers (ProjectileType p_projectileType, Vector3[] p_startPoints, Vector3 p_finishPoint, float p_travelTime, int p_firstPerpFlag, int p_numberOfSegments_Min, int p_numberOfSegments_Max)
	{
		// Выберем projectile prefab из массива
		GameObject l_projectilePrefab = projectilesPrefabs[0].projectile;
		for (int i_prefab = 0; i_prefab < projectilesPrefabs.Length; i_prefab++)
		{
			if (projectilesPrefabs[i_prefab].type == p_projectileType)
			{
				l_projectilePrefab = projectilesPrefabs[i_prefab].projectile;
				break;
			}

			else if (i_prefab == projectilesPrefabs.Length - 1)
				Debug.Log ("p_projectileType <" + p_projectileType + "> not found in projectilesPrefabs[]");
		}

		// >>> ===================== >>>
		// Создание walkers

		BezierWalkerWithTime[] walkers = new BezierWalkerWithTime[p_startPoints.Length];
		for (int i_walker = 0; i_walker < walkers.Length; i_walker++)
		{
			walkers[i_walker] = Instantiate (l_projectilePrefab, p_startPoints[i_walker], Quaternion.identity).AddComponent<BezierWalkerWithTime> ();
			walkers[i_walker].gameObject.name = "Projectile";

			walkers[i_walker].travelTime = p_travelTime;
			walkers[i_walker].movementLerpModifier = movementLerpModifier;
			walkers[i_walker].rotationLerpModifier = rotationLerpModifier;
			walkers[i_walker].lookForward = lookForward;

			walkers[i_walker].spline = CreateSpline (p_startPoints[i_walker], p_finishPoint, p_firstPerpFlag, p_numberOfSegments_Min, p_numberOfSegments_Max);
		}

		return walkers;
	}

	private BezierSpline CreateSpline (Vector3 p_startPoint, Vector3 p_finishPoint, int p_firstPerpFlag, int p_numberOfSegments_Min, int p_numberOfSegments_Max)
	{
		BezierSpline splineInst = new GameObject ("Spline").AddComponent<BezierSpline> ();

		List<Vector3> listOfPoints = new List<Vector3> ();
		listOfPoints.Add (p_startPoint);
		listOfPoints.Add (p_finishPoint);

		listOfPoints.InsertRange (1, CreateSwirlPoints (p_startPoint, p_finishPoint, p_firstPerpFlag, p_numberOfSegments_Min, p_numberOfSegments_Max));
		splineInst.transform.position = listOfPoints[0];

		foreach (Vector3 point in listOfPoints)
		{
			GameObject pointInst = new GameObject ("Point");
			pointInst.transform.parent = splineInst.transform;
			pointInst.transform.position = point;
			pointInst.AddComponent<BezierPoint> ();
		}

		splineInst.Refresh ();
		splineInst.AutoConstructSpline ();

		return splineInst;
	}

	private Vector3[] CreateSwirlPoints (Vector3 p_startPoint, Vector3 p_finishPoint, int p_firstPerpFlag, int p_numberOfSegments_Min, int p_numberOfSegments_Max)
	{
		if (p_numberOfSegments_Max < p_numberOfSegments_Min)
			p_numberOfSegments_Max = p_numberOfSegments_Min;

		int numberOfSegments = Random.Range (p_numberOfSegments_Min, p_numberOfSegments_Max + 1);
		if (numberOfSegments > 0)
		{
			Vector3[] points = new Vector3[numberOfSegments];

			bool perpFlag;

			if (p_firstPerpFlag == 0)
				perpFlag = false;
			else if (p_firstPerpFlag == 1)
				perpFlag = true;
			else
				perpFlag = Random.Range (0, 2) > 0;

			for (int i_point = 0; i_point < points.Length; i_point++)
			{
				Vector3 perpCenter = Vector3.Lerp (p_startPoint, p_finishPoint, (i_point + 1) / (float) (numberOfSegments + 1));
				float distance = Vector3.Distance (perpCenter, p_finishPoint);

				points[i_point] = (perpFlag) ?
					perpCenter + Quaternion.AngleAxis (0, Vector3.forward) * Vector3.right * distance / 4 :
					perpCenter + Quaternion.AngleAxis (180, Vector3.forward) * Vector3.right * distance / 4;

				perpFlag = !perpFlag;
			}

			return points;
		}

		else
			return new Vector3[0];
	}
}