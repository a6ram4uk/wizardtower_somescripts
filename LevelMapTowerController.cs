﻿using UnityEngine;

[System.Serializable]
public enum ScrollDirection { None, Up, Down }

public class LevelMapTowerController : MonoBehaviour
{
	private void Awake () { tag = ProjectTags.t_Tower; }

	[SerializeField] private Camera _Camera;
	[SerializeField] private Transform _Controller; // Child двигаемое "тело" башни;
	private BoxCollider selfBoxCollider;

	[SerializeField] private float translateSpeedFactor = 10.0f;
	private float rotateSpeedFactor = 0.0f;

	[SerializeField, Range (0.0f, 50.0f)] private float rotateSpeedCoefficient = 30.0f;
	[SerializeField] private bool invertRotate = false;
	[SerializeField, Range (0.0f, 5.0f)] private float dragCoefficient = 0.05f;

	[SerializeField] private float translateMaxSpeed = 1.5f;
	private float rotateMaxSpeed = 0;

	[SerializeField] private bool restrictMovement = false;
	[SerializeField] private float towerTranslateUpperBound_Y;
	[SerializeField] private float towerRotateUpperBound_Y;
	[SerializeField] private float towerTranslateLowerBound_Y;
	[SerializeField] private float towerRotateLowerBound_Y;

	private bool isTouched = false;
	private Vector2? prevTouchPos = null;
	private Vector2? currTouchPos = null;

	private ScrollDirection scrollDirection;

	private float translateSpeed = 0.0f;
	private float rotateSpeed = 0.0f;

	private float translateDrag = 0.0f;
	private float rotateDrag = 0.0f;

	// >>> ============================================= >>>

	// Коснулись башни
	public void StartTowerControl ()
	{
		isTouched = true;
		prevTouchPos = _Camera.ScreenToViewportPoint (Input.mousePosition);
	}

	// Отпустили башню
	public void StopTowerControl ()
	{
		isTouched = false;
		prevTouchPos = null;
		currTouchPos = null;
	}

	private void FixedUpdate ()
	{
		rotateSpeedFactor = translateSpeedFactor * rotateSpeedCoefficient;
		rotateMaxSpeed = translateMaxSpeed * rotateSpeedCoefficient;

		translateDrag = translateSpeedFactor * dragCoefficient;
		rotateDrag = rotateSpeedFactor * dragCoefficient;

		TowerControl ();
	}

	private void TowerControl ()
	{
		if (isTouched)
		{
			currTouchPos = _Camera.ScreenToViewportPoint (Input.mousePosition);

			if (currTouchPos == prevTouchPos)
			{
				scrollDirection = ScrollDirection.None;
				translateSpeed = 0.0f;
				rotateSpeed = 0.0f;
				return;
			}

			if (currTouchPos.Value.y > prevTouchPos.Value.y)
				scrollDirection = ScrollDirection.Up;
			else
				scrollDirection = ScrollDirection.Down;

			// Изначально diff_Y высчитывается в диаппазоне (0, 1) от размеров экрана,
			// умножим результат на 100, что бы перевести его в %, для упрощения рассчетов.
			float diff_Y = Mathf.Abs ((float) ((currTouchPos.Value.y - prevTouchPos.Value.y) * 100));

			translateSpeed = Mathf.Clamp (diff_Y * translateSpeedFactor * Time.deltaTime, 0.0f, translateMaxSpeed);
			rotateSpeed = Mathf.Clamp (diff_Y * rotateSpeedFactor * Time.deltaTime, 0.0f, rotateMaxSpeed);

			TowerScroll ();

			prevTouchPos = currTouchPos;
			return;
		}

		if (translateSpeed == 0.0f)
		{
			scrollDirection = ScrollDirection.None;
			translateSpeed = 0.0f;
			rotateSpeed = 0.0f;
			return;
		}

		// Торможение
		translateSpeed = Mathf.Clamp (translateSpeed - translateDrag * Time.deltaTime, 0.0f, translateSpeed);
		rotateSpeed = Mathf.Clamp (rotateSpeed - rotateDrag * Time.deltaTime, 0.0f, rotateSpeed);

		TowerScroll ();
	}

	private void TowerScroll ()
	{
		if (invertRotate)
			rotateSpeed *= -1;

		if (scrollDirection == ScrollDirection.Up)
		{
			if (restrictMovement)
			{
				if (_Controller.transform.localPosition.y < towerTranslateUpperBound_Y)
				{
					_Controller.transform.Translate (new Vector3 (0, translateSpeed, 0), Space.Self);
					_Controller.transform.Rotate (new Vector3 (0, rotateSpeed, 0), Space.Self);

					if (_Controller.transform.localPosition.y > towerTranslateUpperBound_Y)
					{
						_Controller.transform.localPosition =
							new Vector3 (_Controller.transform.localPosition.x, towerTranslateUpperBound_Y, _Controller.transform.localPosition.z);
						_Controller.transform.localEulerAngles =
							new Vector3 (_Controller.transform.localEulerAngles.x, towerRotateUpperBound_Y, _Controller.transform.localEulerAngles.z);

						scrollDirection = ScrollDirection.None;
					}
				}
			}

			else
			{
				_Controller.transform.Translate (new Vector3 (0, translateSpeed, 0), Space.Self);
				_Controller.transform.Rotate (new Vector3 (0, rotateSpeed, 0), Space.Self);
			}
		}

		else if (scrollDirection == ScrollDirection.Down)
		{
			if (restrictMovement)
			{
				if (_Controller.transform.localPosition.y > towerTranslateLowerBound_Y)
				{
					_Controller.transform.Translate (new Vector3 (0, -translateSpeed, 0), Space.Self);
					_Controller.transform.Rotate (new Vector3 (0, -rotateSpeed, 0), Space.Self);

					if (_Controller.transform.localPosition.y < towerTranslateLowerBound_Y)
					{
						_Controller.transform.localPosition =
							new Vector3 (_Controller.transform.localPosition.x, towerTranslateLowerBound_Y, _Controller.transform.localPosition.z);
						_Controller.transform.localEulerAngles =
							new Vector3 (_Controller.transform.localEulerAngles.x, towerRotateLowerBound_Y, _Controller.transform.localEulerAngles.z);

						scrollDirection = ScrollDirection.None;
					}
				}
			}

			else
			{
				_Controller.transform.Translate (new Vector3 (0, -translateSpeed, 0), Space.Self);
				_Controller.transform.Rotate (new Vector3 (0, -rotateSpeed, 0), Space.Self);
			}
		}
	}
}