﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// >>> =========================================== >>>
// Data Types

[System.Serializable]
public enum WizardAnimation { None, Idle, AppearLevel, AppearMenu, TowerAppear, Shooting, Restoring, Mixing, Fear, Hello, Dancing, Staff, Break, Mustache, LookAround }

[System.Serializable]
public enum WizardAppearAnimation { None, AppearLevel, AppearMenu }

[System.Serializable]
public struct WizardAnimationPlayCount { public WizardAnimation animation; public int playCount; }

// >>> =========================================== >>>

[RequireComponent (typeof (Animator))]
public class WizardController : MonoBehaviour
{
	// >>> ======================================= >>>
	// Animator keys

	private const string key_AppearNone = "AppearNone";
	private const string key_AppearLevel = "AppearLevel";
	private const string key_AppearMenu = "AppearMenu";
	private const string key_TowerAppear = "TowerAppear";
	private const string key_Shooting = "Shooting";
	private const string key_Restoring = "Restoring";
	private const string key_Mixing = "Mixing";
	private const string key_Fear = "Fear";
	private const string key_Hello = "Hello";
	private const string key_Dancing = "Dancing";
	private const string key_Staff = "Staff";
	private const string key_Break = "Break";
	private const string key_Mustache = "Mustache";
	private const string key_LookAround = "LookAround";

	// >>> ======================================= >>>
	// Variables

	private Animator wizardAnimator;
	private void Awake ()
	{
		tag = ProjectTags.t_Wizard;

		if (GetComponent<Animator> ())
			wizardAnimator = GetComponent<Animator> ();
	}

	private void Start ()
	{
		if (wizardStaffTopPoint && SendProjectilesManager.Instance)
			SendProjectilesManager.Instance.bossStartPathPoint = wizardStaffTopPoint;
	}

	[SerializeField] private Transform wizardStaffTopPoint;
	[SerializeField] private Transform wizardStaffBottomPoint;
	[SerializeField] private Renderer[] wizardRenderers;

	// >>> ------------------------- >>>
	// Эффекты частит

	[Header ("Ссылки на префабы эффектов частиц:")]
	[SerializeField] private GameObject towerAppearEffect;
	[SerializeField] private GameObject startShootingEffect;
	[SerializeField] private GameObject shootingHitMomentEffect;
	[SerializeField] private GameObject startRestoringEffect;
	[SerializeField] private GameObject restoringHitMomentEffect;
	[SerializeField] private GameObject startMixingMiddleEffect;
	[SerializeField] private GameObject startMixingStaffEffect;

	// >>> ------------------------- >>>
	// Анимационная составляющая

	[Header ("Из какой анимации маг должен появиться в первый раз?")]
	public WizardAppearAnimation wizardAppearAnimation = WizardAppearAnimation.None;

	[Header ("Данные об анимациях:")]
	[SerializeField] private WizardAnimation currWizardAnimation = WizardAnimation.None;

	[SerializeField] private WizardAnimation[] idleWizardAnimations = new WizardAnimation[]
	{
		WizardAnimation.Staff, WizardAnimation.Break, WizardAnimation.Mustache, WizardAnimation.LookAround
	};

	[SerializeField] private WizardAnimation[] specialWizardAnimations = new WizardAnimation[]
	{
		WizardAnimation.Hello, WizardAnimation.Dancing
	};

	[SerializeField] private WizardAnimationPlayCount[] wizardAnimationsPlayLog = new WizardAnimationPlayCount[]
	{
		new WizardAnimationPlayCount () { animation = WizardAnimation.Idle, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.AppearLevel, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.AppearMenu, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.TowerAppear, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Shooting, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Restoring, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Mixing, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Fear, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Hello, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Dancing, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Staff, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Break, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.Mustache, playCount = 0 },
			new WizardAnimationPlayCount () { animation = WizardAnimation.LookAround, playCount = 0 }
	};

	// >>> ------------------------- >>>
	// IEnumerator'ы

	private IEnumerator IE_WaitPlayWizardAnimation;
	private IEnumerator IE_IdleAnimationsQueue;

	// >>> ======================================= >>>
	// Вызовы анимаций

	public void LatePlayWizardAnimation (WizardAnimation p_animation, float p_startDelay)
	{
		if (wizardAnimator)
		{
			if (IE_WaitPlayWizardAnimation != null)
				StopCoroutine (IE_WaitPlayWizardAnimation);

			IE_WaitPlayWizardAnimation = WaitPlayWizardAnimation_Coroutine (p_animation, p_startDelay);
			StartCoroutine (IE_WaitPlayWizardAnimation);
		}
	}

	private IEnumerator WaitPlayWizardAnimation_Coroutine (WizardAnimation p_animation, float p_startDelay)
	{
		yield return new WaitForSeconds (p_startDelay);
		TryPlayWizardAnimation (p_animation);

		IE_WaitPlayWizardAnimation = null;
	}

	public void TryPlayWizardAnimation (WizardAnimation p_animation)
	{
		if (wizardAnimator)
		{
			if (IE_WaitPlayWizardAnimation != null)
				StopCoroutine (IE_WaitPlayWizardAnimation);

			switch (p_animation)
			{
				case WizardAnimation.TowerAppear:
					{
						wizardAnimator.SetTrigger (key_TowerAppear);
						break;
					}

				case WizardAnimation.Shooting:
					{
						wizardAnimator.SetTrigger (key_Shooting);
						break;
					}

				case WizardAnimation.Restoring:
					{
						wizardAnimator.SetTrigger (key_Restoring);
						break;
					}

				case WizardAnimation.Mixing:
					{
						wizardAnimator.SetTrigger (key_Mixing);
						break;
					}

				case WizardAnimation.Fear:
					{
						wizardAnimator.SetTrigger (key_Fear);
						break;
					}

				case WizardAnimation.Hello:
					{
						wizardAnimator.SetTrigger (key_Hello);
						break;
					}

				case WizardAnimation.Dancing:
					{
						wizardAnimator.SetTrigger (key_Dancing);
						break;
					}

				case WizardAnimation.Staff:
					{
						wizardAnimator.SetTrigger (key_Staff);
						break;
					}

				case WizardAnimation.Break:
					{
						wizardAnimator.SetTrigger (key_Break);
						break;
					}

				case WizardAnimation.Mustache:
					{
						wizardAnimator.SetTrigger (key_Mustache);
						break;
					}

				case WizardAnimation.LookAround:
					{
						wizardAnimator.SetTrigger (key_LookAround);
						break;
					}
			}
		}
	}

	// >>> ======================================= >>>
	// Логика появления и очередность воспроизведения Idle анимаций

	private void OnEnable ()
	{
		DeactivateAllWizardRenderers ();

		switch (wizardAppearAnimation)
		{
			case WizardAppearAnimation.None:
				wizardAnimator.SetTrigger (key_AppearNone);
				break;

			case WizardAppearAnimation.AppearLevel:
				wizardAnimator.SetTrigger (key_AppearLevel);
				break;

			case WizardAppearAnimation.AppearMenu:
				wizardAnimator.SetTrigger (key_AppearMenu);
				break;
		}

		StartIdleAnimationsQueue ();
	}

	private void OnDisable ()
	{
		if (IE_IdleAnimationsQueue != null)
		{
			StopCoroutine (IE_IdleAnimationsQueue);
			IE_IdleAnimationsQueue = null;
		}

		DeactivateAllWizardRenderers ();
	}

	private bool renderersActivated = true;
	private void ActivateAllWizardRenderers ()
	{
		if (!renderersActivated)
		{
			StartCoroutine (ActivateAllWizardRenderers_Coroutine ());
			renderersActivated = true;
		}
	}

	private IEnumerator ActivateAllWizardRenderers_Coroutine ()
	{
		yield return new WaitForEndOfFrame ();

		for (int i_renderer = 0; i_renderer < wizardRenderers.Length; i_renderer++)
			wizardRenderers[i_renderer].enabled = true;
	}

	private void DeactivateAllWizardRenderers ()
	{
		if (renderersActivated)
		{
			for (int i_renderer = 0; i_renderer < wizardRenderers.Length; i_renderer++)
				wizardRenderers[i_renderer].enabled = false;

			renderersActivated = false;
		}
	}

	private void StartIdleAnimationsQueue ()
	{
		if (IE_IdleAnimationsQueue != null)
			StopCoroutine (IE_IdleAnimationsQueue);

		IE_IdleAnimationsQueue = IdleAnimationsQueue_Coroutine ();
		StartCoroutine (IE_IdleAnimationsQueue);
	}

	private IEnumerator IdleAnimationsQueue_Coroutine ()
	{
		while (true)
		{
			WizardAnimation l_nextIdleAnimation = GetWizardAnimationWithLowerPlayCount (idleWizardAnimations);

			int l_nextIdleIndexInPlayLog = -1;
			int l_nextIdlePlayCount = -1;

			for (int i_record = 0; i_record < wizardAnimationsPlayLog.Length; i_record++)
			{
				if (wizardAnimationsPlayLog[i_record].animation == l_nextIdleAnimation)
				{
					l_nextIdleIndexInPlayLog = i_record;
					l_nextIdlePlayCount = wizardAnimationsPlayLog[i_record].playCount;

					break;
				}
			}

			if (l_nextIdlePlayCount == -1)
			{
				// Если l_nextIdlePlayCount остался равен -1, значит этого <Idle> нет в PlayLog[] => останавливаем весь механизм.
				Debug.Log ("Выбранная Idle_" + l_nextIdleAnimation + " анимация не найдена в WizardAnimationsPlayLog[].");
				break;
			}

			// Минимальное время между запусками Idle анимаций.
			yield return new WaitForSeconds (5.0f);

			TryPlayWizardAnimation (l_nextIdleAnimation);

			// Ждем пока "выбранная" анимация запуститься (ее playCount станет +1).
			yield return new WaitUntil (() => l_nextIdlePlayCount < wizardAnimationsPlayLog[l_nextIdleIndexInPlayLog].playCount);
		}

		IE_IdleAnimationsQueue = null;
	}

	private WizardAnimation GetWizardAnimationWithLowerPlayCount (WizardAnimation[] p_animations)
	{
		WizardAnimation l_result = p_animations[Random.Range (0, p_animations.Length)];
		WizardAnimationPlayCount[] t_animations = new WizardAnimationPlayCount[p_animations.Length];

		// Вытащим данные о вызовах запрашиваемых анимаций из журнала.
		for (int i_animation = 0; i_animation < p_animations.Length; i_animation++)
		{
			for (int i_record = 0; i_record < wizardAnimationsPlayLog.Length; i_record++)
			{
				if (p_animations[i_animation] == wizardAnimationsPlayLog[i_record].animation)
				{
					t_animations[i_animation].animation = wizardAnimationsPlayLog[i_record].animation;
					t_animations[i_animation].playCount = wizardAnimationsPlayLog[i_record].playCount;
					break;
				}

				else if (i_record == wizardAnimationsPlayLog.Length - 1)
					Debug.Log ("Анимация <" + p_animations[i_animation] + "> не найдена в WizardAnimationsPlayLog[].");
			}
		}

		// Найдем самый меньший playCount среди отобранных анимаций.
		int l_lowerPlayCount = t_animations[0].playCount;
		for (int i_animation = 0; i_animation < t_animations.Length; i_animation++)
		{
			if (t_animations[i_animation].playCount < l_lowerPlayCount)
				l_lowerPlayCount = t_animations[i_animation].playCount;
		}

		// Найдем анимации, playCount которых равен меньшему.
		List<WizardAnimation> r_animations = new List<WizardAnimation> ();

		for (int i_animation = 0; i_animation < t_animations.Length; i_animation++)
		{
			if (t_animations[i_animation].playCount == l_lowerPlayCount)
				r_animations.Add (t_animations[i_animation].animation);
		}

		l_result = r_animations[Random.Range (0, r_animations.Count)];
		return l_result;
	}

	// >>> ======================================= >>>
	// Воспроизведение анимаций по клику

	private bool waitUntilPreviousStarted = false;
	private float lastSpecialAnimationStartTime = 0.0f;
	private float minDelayBetweenSpecialAnimations = 5.0f;

	public void ClickOnTheWizard ()
	{
		if (Time.time - lastSpecialAnimationStartTime > minDelayBetweenSpecialAnimations)
		{
			if (currWizardAnimation == WizardAnimation.Idle ||
				currWizardAnimation == WizardAnimation.Staff ||
				currWizardAnimation == WizardAnimation.Break ||
				currWizardAnimation == WizardAnimation.Mustache ||
				currWizardAnimation == WizardAnimation.LookAround)
			{
				if (!waitUntilPreviousStarted)
				{
					StartCoroutine (PlaySpecialAnimation_Coroutine ());
					lastSpecialAnimationStartTime = Time.time;
				}
			}

			else
				Debug.Log ("Анимацию <" + currWizardAnimation + "> нельзя прервать.");
		}

		else
			Debug.Log ("Не так часто! Дай мне " + (minDelayBetweenSpecialAnimations - (Time.time - lastSpecialAnimationStartTime)) + " секунд...");
	}

	private IEnumerator PlaySpecialAnimation_Coroutine ()
	{
		waitUntilPreviousStarted = true;

		WizardAnimation l_animation = GetWizardAnimationWithLowerPlayCount (specialWizardAnimations);

		int l_animationIndexInPlayLog = -1;
		int l_animationPlayCount = -1;

		for (int i_record = 0; i_record < wizardAnimationsPlayLog.Length; i_record++)
		{
			if (wizardAnimationsPlayLog[i_record].animation == l_animation)
			{
				l_animationIndexInPlayLog = i_record;
				l_animationPlayCount = wizardAnimationsPlayLog[i_record].playCount;

				break;
			}
		}

		if (l_animationPlayCount == -1)
			// Если l_animationPlayCount остался равен -1, значит этого <Special> нет в PlayLog[].
			Debug.Log ("Выбранная Special_" + l_animation + " анимация не найдена в WizardAnimationsPlayLog[].");

		else
		{
			TryPlayWizardAnimation (l_animation);

			// Ждем пока "выбранная" анимация запуститься (ее playCount станет +1).
			yield return new WaitUntil (() => l_animationPlayCount < wizardAnimationsPlayLog[l_animationIndexInPlayLog].playCount);
		}

		waitUntilPreviousStarted = false;
	}

	// >>> ======================================= >>>
	// Wizard Animations events

	public void AE_Idle_Start () { ActivateAllWizardRenderers (); currWizardAnimation = WizardAnimation.Idle; IncrementAnimationPlayCount (WizardAnimation.Idle); }
	public void AE_AppearLevel_Start () { ActivateAllWizardRenderers (); currWizardAnimation = WizardAnimation.AppearLevel; IncrementAnimationPlayCount (WizardAnimation.AppearLevel); }
	public void AE_AppearMenu_Start () { ActivateAllWizardRenderers (); currWizardAnimation = WizardAnimation.AppearMenu; IncrementAnimationPlayCount (WizardAnimation.AppearMenu); }
	public void AE_TowerAppear_Start () { currWizardAnimation = WizardAnimation.TowerAppear; IncrementAnimationPlayCount (WizardAnimation.TowerAppear); }
	public void AE_TowerAppear_Middle () { CreateEffect (towerAppearEffect, wizardStaffTopPoint, false, 4.5f); }

	[HideInInspector] public bool shootingHitMoment = false;
	public void AE_Shooting_Start () { currWizardAnimation = WizardAnimation.Shooting; IncrementAnimationPlayCount (WizardAnimation.Shooting); CreateEffect (startShootingEffect, wizardStaffTopPoint, false, 1.5f); }
	public void AE_Shooting_Hit () { shootingHitMoment = true; CreateEffect (shootingHitMomentEffect, wizardStaffTopPoint, true); }

	[HideInInspector] public bool restoringHitMoment = false;
	public void AE_Restoring_Start () { currWizardAnimation = WizardAnimation.Restoring; IncrementAnimationPlayCount (WizardAnimation.Restoring); CreateEffect (startRestoringEffect, wizardStaffTopPoint, false, 1.25f); }
	public void AE_Restoring_Hit () { restoringHitMoment = true; CreateEffect (restoringHitMomentEffect, wizardStaffBottomPoint, true); }

	public void AE_Mixing_Start () { currWizardAnimation = WizardAnimation.Mixing; IncrementAnimationPlayCount (WizardAnimation.Mixing); CreateEffect (startMixingMiddleEffect, transform, false); CreateEffect (startMixingStaffEffect, wizardStaffTopPoint, false, 2.0f); }
	public void AE_Fear_Start () { currWizardAnimation = WizardAnimation.Fear; IncrementAnimationPlayCount (WizardAnimation.Fear); }
	public void AE_Hello_Start () { currWizardAnimation = WizardAnimation.Hello; IncrementAnimationPlayCount (WizardAnimation.Hello); }
	public void AE_Dancing_Start () { currWizardAnimation = WizardAnimation.Dancing; IncrementAnimationPlayCount (WizardAnimation.Dancing); }

	public void AE_Staff_Start () { currWizardAnimation = WizardAnimation.Staff; IncrementAnimationPlayCount (WizardAnimation.Staff); }
	public void AE_Break_Start () { currWizardAnimation = WizardAnimation.Break; IncrementAnimationPlayCount (WizardAnimation.Break); }
	public void AE_Mustache_Start () { currWizardAnimation = WizardAnimation.Mustache; IncrementAnimationPlayCount (WizardAnimation.Mustache); }
	public void AE_LookAround_Start () { currWizardAnimation = WizardAnimation.LookAround; IncrementAnimationPlayCount (WizardAnimation.LookAround); }

	private void IncrementAnimationPlayCount (WizardAnimation p_animation)
	{
		for (int i_animation = 0; i_animation < wizardAnimationsPlayLog.Length; i_animation++)
		{
			if (wizardAnimationsPlayLog[i_animation].animation == p_animation)
			{
				wizardAnimationsPlayLog[i_animation].playCount++;
				break;
			}
		}
	}

	private void CreateEffect (GameObject p_effect, Transform p_target, bool p_outside, float p_lifeTime = 3.0f)
	{
		if (!p_effect || !p_target)
		{
			if (!p_effect)
				Debug.Log ("p_effect == null");
			if (!p_target)
				Debug.Log ("p_parent == null");
			return;
		}

		GameObject l_effectInst = null;

		if (p_outside)
			l_effectInst = Instantiate (p_effect, p_target.position, Quaternion.identity);
		else
			l_effectInst = Instantiate (p_effect, p_target);

		if (l_effectInst.GetComponent<ParticleSystem> ())
			if (!l_effectInst.GetComponent<ParticleSystem> ().main.playOnAwake)
				l_effectInst.GetComponent<ParticleSystem> ().Play ();

		Destroy (l_effectInst, p_lifeTime);
	}
}