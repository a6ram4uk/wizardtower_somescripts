﻿// Компонент, ждущий удара взрывной волны и создающий вибрацию объекта;

using UnityEngine;

public class ReactionToExplosionController : MonoBehaviour
{
	[SerializeField] private float objectMass;
	[SerializeField] private int pointsCount;

	private Vector3[] shakePoints;

	private Vector3 defaultSelfPosition;
	private Vector3 explosionPosition;
	private Vector3 explosionMirrorPoint;

	private float distanceToFurthestPoint;

	private float startSpeed;
	private float finishSpeed = 0f;
	private float currSpeed;
	private float acceleration;

	private float maxDeltaPercent;
	private float deltaDist = 0;

	private bool isShake;

	// >>> ================================================ >>>
	// Calculations

	public void DetectedExplosion (Vector3 p_explosionPosition, float p_explosionForce, float p_objectDistModifier, float p_objectSpeedModifier, float p_maxDeltaPercent, float p_finishSpeedPercent)
	{
		if (!isShake)
		{
			defaultSelfPosition = transform.position;
			explosionPosition = p_explosionPosition;
			explosionMirrorPoint = transform.position - (explosionPosition - transform.position);

			if (objectMass <= 0)
				objectMass = 0.01f;

			distanceToFurthestPoint = p_explosionForce / objectMass * p_objectDistModifier;
			startSpeed = p_explosionForce / objectMass * p_objectSpeedModifier;
			finishSpeed = startSpeed / 100 * p_finishSpeedPercent;
			currSpeed = startSpeed;

			shakePoints = CalculateShakePoints ();
			acceleration = CalculateAcceleration ();

			maxDeltaPercent = p_maxDeltaPercent;
			deltaDist = Vector3.Distance (defaultSelfPosition, shakePoints[targetPointId]) / 100 * maxDeltaPercent;

			isShake = true;
		}
	}

	private Vector3[] CalculateShakePoints ()
	{
		Vector3[] points = new Vector3[pointsCount + 1];

		Vector3 _X = transform.position;
		Vector3 _A = explosionMirrorPoint;
		Vector3 _B = explosionPosition;

		float unit = 0.1f;
		bool reverse = false;

		float unit_A_perc = unit * 100 / Vector3.Distance (_X, _A);
		float unit_B_perc = unit * 100 / Vector3.Distance (_X, _B);
		Vector3 unit_A_pos = Vector3.Lerp (_X, _A, unit_A_perc / 100);
		Vector3 unit_B_pos = Vector3.Lerp (_X, _B, unit_B_perc / 100);

		int calculated = 0;
		while (calculated < pointsCount)
		{
			float range = distanceToFurthestPoint - (distanceToFurthestPoint * calculated / pointsCount);
			if (!reverse)
			{
				Vector3 point = Vector3.LerpUnclamped (_X, unit_A_pos, range);
				points[calculated] = point;
			}

			else
			{
				Vector3 point = Vector3.LerpUnclamped (_X, unit_B_pos, range);
				points[calculated] = point;
			}

			reverse = !reverse;
			calculated++;
		}

		points[pointsCount] = defaultSelfPosition;
		return points;
	}

	private float CalculateAcceleration ()
	{
		float result = 0;

		float allPath = 0;
		allPath += Vector3.Distance (defaultSelfPosition, shakePoints[0]);
		for (int i_point = 0; i_point < shakePoints.Length; i_point++)
			if (i_point < shakePoints.Length - 1)
				allPath += Vector3.Distance (shakePoints[i_point], shakePoints[i_point + 1]);

		result = (Mathf.Pow (finishSpeed, 2) - Mathf.Pow (startSpeed, 2)) / (allPath * 2);
		return result;
	}

	// >>> ================================================ >>>
	// Shaking

	private void Update () { Shake (); }

	private int targetPointId = 0;
	private void Shake ()
	{
		if (isShake)
		{
			if (Vector3.Distance (transform.position, shakePoints[targetPointId]) > deltaDist)
			{
				transform.position = Vector3.MoveTowards (transform.position, shakePoints[targetPointId], currSpeed * Time.deltaTime);
				currSpeed = Mathf.Clamp (currSpeed + acceleration * Time.deltaTime, finishSpeed, currSpeed);

				if (currSpeed <= finishSpeed)
					ResetShake ();
			}

			else
			{
				if (targetPointId < shakePoints.Length - 1)
				{
					targetPointId++;
					deltaDist = Vector3.Distance (defaultSelfPosition, shakePoints[targetPointId]) / 100 * maxDeltaPercent;
				}

				else
					ResetShake ();
			}
		}
	}

	private void ResetShake ()
	{
		if (isShake)
		{
			isShake = false;
			targetPointId = 0;
			transform.position = defaultSelfPosition;
		}
	}
}