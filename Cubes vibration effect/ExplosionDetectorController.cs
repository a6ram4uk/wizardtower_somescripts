﻿// Sphere collider, представляющий собой распространяющуюся взрывную волну,
// спавнящуюся в месте взрыва;

using UnityEngine;

public class ExplosionDetectorController : MonoBehaviour
{
	[Header ("Максимальный радиус распространения волны.")]
	public float explosionWaveRadius = 5;
	[Header ("Скорость распространения волны.")]
	public float explosionWaveSpeed = 10;
	[Header ("Начальное значение силы. Затухает с распространением волны. В момент пика волны == 0.")]
	public float explosionWaveForce = 1;
	[Header ("Модификаторы для \"баланса\" величин \"отскока\" и \"скорости\" объекта по отношению друг к другу.")]
	[Range (0, 1)] public float objectDistModifier = 0.5f;
	[Range (0, 1)] public float objectSpeedModifier = 0.5f;
	[Header ("Максимальный % от дистанции \"А => Б\", при котором Б считается достигнутой объектом.")]
	[Range (0, 10)] public float maxDeltaPercent = 5;
	[Header ("% от начальной скорости, ниже которого скорость объекта не упадет.")]
	[Range (0, 10)] public float finishSpeedPercent = 5;

	private float explosionWaveForceTemp;
	private float explosionWaveForceDrag;

	// >>> ================================================ >>>
	// Recalculate

	private void OnEnable ()
	{
		explosionWaveForceTemp = explosionWaveForce;
		explosionWaveForceDrag = explosionWaveForce * explosionWaveSpeed / explosionWaveRadius;
	}

	private void Start ()
	{
		explosionWaveForceTemp = explosionWaveForce;
		explosionWaveForceDrag = explosionWaveForce * explosionWaveSpeed / explosionWaveRadius;
	}

	// >>> ================================================ >>>

	private void Update () { DetectorSizeController (); }

	private void DetectorSizeController ()
	{
		float currRadius = gameObject.GetComponent<SphereCollider> ().radius;

		if (currRadius < explosionWaveRadius)
		{
			currRadius = Mathf.Clamp (currRadius + explosionWaveSpeed * Time.deltaTime, currRadius, explosionWaveRadius);
			gameObject.GetComponent<SphereCollider> ().radius = currRadius;

			explosionWaveForceTemp = Mathf.Clamp (explosionWaveForceTemp - explosionWaveForceDrag * Time.deltaTime, 0, explosionWaveForceTemp);
		}

		else
			Destroy (gameObject, 0.5f);
	}

	private void OnTriggerEnter (Collider p_collision)
	{
		if (p_collision.gameObject.GetComponent<ReactionToExplosionController> ())
		{
			p_collision.gameObject.GetComponent<ReactionToExplosionController> ()
				.DetectedExplosion (transform.position, explosionWaveForceTemp, objectDistModifier, objectSpeedModifier, maxDeltaPercent, finishSpeedPercent);
		}
	}
}